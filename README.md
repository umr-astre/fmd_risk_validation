
<!-- README.md is generated from README.Rmd. Please edit that file -->

# FMD risk validation - Tunisia

<!-- badges: start -->

<!-- badges: end -->

Renaud Lancelot, Facundo Muñoz

January 2020

Validation of a qualitative risk assessment of Foot-and-Mouth Disease in
Tunisia with observed data.

Full report: <https://umr-astre.pages.mia.inra.fr/fmd_risk_validation/>

![](README_files/figure-gfm/cases-and-risk-1.png)<!-- -->
![](README_files/figure-gfm/freq-expo-1.png)<!-- -->
